#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
@interface ObjectView : UIView {
    @public float dx, dy, da, angle;  // motion
    @public int size;
}
-(void)move;
-(void)rotate: (float)_angle;
-(void)setVel: (float)_dx y:(float)_dy;
-(void)setRotVel: (float)_da;
@end
