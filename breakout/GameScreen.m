#import "GameScreen.h"
#import "ObjectView.h"
#import <AVFoundation/AVFoundation.h>

#define PI 3.1415926535
#define D2R(x) ((x) / 180.0 * PI)
#define R2D(x) ((x) / PI * 180.0)

#define FPS 60.0
#define SHIP_MOVE_SPEED 60.0
#define SHIP_CONTROL_SPEED 1.5
#define SHIP_TAP_SENSITIVITY 10
#define BULLET_MOVE_SPEED 5

#define ASTEROID_SIZE 120
#define ASTEROID_MIN_SIZE ASTEROID_SIZE/4
#define ASTEROID_SPEED 0.5
#define ASTEROID_ROT_SPEED 0.01
#define ASTEROID_SPEEDUP 1.5

@interface GameScreen ()
@property (assign) SystemSoundID sound;
@property(nonatomic, strong) AVAudioPlayer *backgroundMusic;
@end

@implementation GameScreen
@synthesize ship;
@synthesize timer;
@synthesize astrs;
@synthesize bullets;
@synthesize scoreLB, livesLB, gameOver, gamingScreen;
@synthesize scoreFinal, playAgainBt;

-(void)createPlayField {
    gameOver.hidden = YES;
    gamingScreen.hidden = NO;
    score = 0;
    [scoreLB setText:@"0"];
    astrs = [[NSMutableArray alloc] init];
    bullets = [[NSMutableArray alloc] init];

    // make ship
    ship = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 40, 43)];
    UIImageView *bg =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Spaceship.png"]];
    bg.frame = ship.bounds;
    [gamingScreen addSubview:ship];
    [ship addSubview:bg];
    
    //sets the screen for gamePlay
    [self reset];
}

-(void)reset {
    // clean all remaining asteroids and bullets
    for(ObjectView* astr in astrs) {
        [astr removeFromSuperview];
    }
    for(ObjectView* bullet in bullets) {
        [bullet removeFromSuperview];
    }
    [astrs removeAllObjects];
    [bullets removeAllObjects];

    // add first asteroids
    [self addAstr];
    [self addAstr];

    // reset the ship
    CGRect bounds = [gamingScreen bounds];
    float y = bounds.size.height - 50;
    float x = (bounds.size.width)/2;
    target = CGPointMake(x, y-10.0);
    [ship setCenter: CGPointMake(x, y)];

    //resetting the lives back to 3
    lives = 3;
    [livesLB setText:@"3"];

   
}

-(void)addAstr{
    int x = arc4random_uniform(400);
    int y = arc4random_uniform(500);
    float dx = arc4random_uniform(ASTEROID_SPEED * 2000)/2000.0 - ASTEROID_SPEED/2;
    float dy = arc4random_uniform(ASTEROID_SPEED * 2000)/2000.0 - ASTEROID_SPEED/2;
    float da = arc4random_uniform(ASTEROID_ROT_SPEED * 2000)/1000.0 - ASTEROID_ROT_SPEED;

    // spawn asteroids off screen at the top/left edge
    // because of the random dx/dy, they'll have a 50/50 shot at looping
    // to the other side anyway, so we don't need to spawn
    // at the right/bottom edges
    int side = arc4random_uniform(2);
    if(side == 0) {
        y = -ASTEROID_SIZE + 1;
    }
    else {
        x = -ASTEROID_SIZE + 1;
    }

    // insure the asteroids are moving at a minimum speed
    dx += dx / fabs(dx) * ASTEROID_SPEED / 2;
    dy += dy / fabs(dy) * ASTEROID_SPEED / 2;

    [self addAstrWithProps: ASTEROID_SIZE x:x y:y dx:dx dy:dy da:da];
}

-(void)addAstrWithProps: (int)size x:(float)x y:(float)y dx:(float)dx dy:(float)dy da:(float)da {
    int j = arc4random_uniform(3);
    NSString *astName;
    //select random image for asteroid
    switch (j) {
        default:
            astName = @"as1.png";
            break;
        case 1:
            astName = @"as2.png";
            break;
        case 2:
            astName = @"as3.png";
            break;
        case 3:
            astName = @"as4.png";
            break;
    }

    // make a test asteroid
    ObjectView* astr = [[ObjectView alloc] initWithFrame: CGRectMake(x, y, size, size)];
    UIImageView *bg1 =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:astName]];
    bg1.frame = astr.bounds;
    [gamingScreen addSubview:astr];
    [astr addSubview:bg1];

    // set velocity / rotation
    [astr setCenter: CGPointMake(x, y)];
    [astr setVel: dx y: dy];
    [astr setRotVel: da];
    [astrs addObject: astr];
    astr->size = size;
}

-(void)rotate: (float)angle {
    ship.transform = CGAffineTransformMakeRotation(angle);
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(!active) {
        return;
    }
    CGPoint cur = [ship center];
    for (UITouch *t in touches) {
        down = [t locationInView: gamingScreen];
        down_target = target;
        // rotate the ship
        float angle = atan2f(down.y - cur.y, down.x - cur.x) + PI/2;
        [self rotate: angle];
        
        // create bullet
        ObjectView* bullet = [[ObjectView alloc] initWithFrame: CGRectMake(cur.x, cur.y, 10, 15)];
        UIImageView* bg =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"beam.png"]];
        bg.frame = bullet.bounds;
        [bullet addSubview:bg];
        
        // set position
        [bullet setVel: BULLET_MOVE_SPEED * cosf(angle - PI/2) y: BULLET_MOVE_SPEED * sinf(angle - PI/2)];
        [bullet rotate: angle];
        
        // add bullet to view / list
        [gamingScreen addSubview:bullet];
        [bullets addObject: bullet];
        
        // play a sound effect when bullet is fired
        [self playSound:@"fire"];
    }
}

-(void)playSound:(NSString*)soundName{ //plays a sound when called with sound name
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"wav"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &_sound);
    AudioServicesPlaySystemSound(self.sound);

}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(!active) {
        return;
    }
    for (UITouch *t in touches) {
        CGPoint cur = [t locationInView: self];
        float dx = cur.x - down.x;
        float dy = cur.y - down.y;
        // only move the ship's target location if the touch has been dragged enough
        if(dx*dx + dy*dy >= SHIP_TAP_SENSITIVITY * SHIP_TAP_SENSITIVITY) {
            target = CGPointMake(down_target.x + dx*SHIP_CONTROL_SPEED, down_target.y + dy*SHIP_CONTROL_SPEED);
        }
    }
}

-(void)startAnimation {
    active = true;
    //create the gamePLay screen
    [self createPlayField];
    //triggers timer event
    timer = [NSTimer scheduledTimerWithTimeInterval:1/FPS  target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
    //play a background music in game
    NSURL *musicFile = [[NSBundle mainBundle] URLForResource:@"level1"
                                               withExtension:@"mp3"];
    self.backgroundMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile
                                                                  error:nil];
    self.backgroundMusic.numberOfLoops = -1;
    [self.backgroundMusic play];
}

-(IBAction)stopAnimation:(id)sender {
    active = false;
    [timer invalidate];
}

-(void)timerEvent:(id)sender {
    CGRect bounds = [self bounds];

    // handle ship motion
    CGPoint cur = [ship center];
    float dx = (target.x - cur.x) / SHIP_MOVE_SPEED;
    float dy = (target.y - cur.y) / SHIP_MOVE_SPEED;
    if(dx * dx + dy * dy >= 1) {
        float angle = atan2f(dy, dx) + PI/2;
        [self rotate: angle];

        cur.x += dx;
        cur.y += dy;

        [ship setCenter: cur];
    }

    // handle bullet motion
    for(NSUInteger x=0; x<bullets.count; x++) {
        // move the bullet
        ObjectView* bullet = bullets[x];
        [bullet move];
        CGPoint loc = [bullet center];

        // remove the bullet if it's out of bounds
        if(loc.x < -10 || loc.x > bounds.size.width+10 || loc.y < -10 || loc.y > bounds.size.height+10) {
            [bullets removeObjectAtIndex: x];
            [bullet removeFromSuperview];
            x--;
        }
    }

    // handle asteroid motion
    for(NSUInteger x=0; x<astrs.count; x++) {
        // move the asteroid
        ObjectView* astr = astrs[x];
        [astr move];
        CGPoint loc = [astr center];

        // loop the asteroid to the other side
        if(loc.x < -astr->size || loc.x > bounds.size.width+astr->size) {
            loc.x = bounds.size.width - loc.x;
        }
        if(loc.y < -astr->size || loc.y > bounds.size.height+astr->size) {
            loc.y = bounds.size.height - loc.y;
        }

        [astr setCenter: loc];
    }

    // check for bullet-asteroid collisions
    for(NSUInteger a=0; a<astrs.count; a++) {
        for(NSUInteger b=0; b<bullets.count; b++) {
            ObjectView* astr = astrs[a];
            ObjectView* bullet = bullets[b];
            
            if(CGRectContainsRect([astr frame], [bullet frame])) {
                // remove them
                [astr removeFromSuperview];
                [astrs removeObjectAtIndex: a];
                [bullet removeFromSuperview];
                [bullets removeObjectAtIndex: b];
                [self playSound:@"bangMedium"];

                score += 10;
                scoreLB.text = [NSString stringWithFormat:@"%d", score];

                if(astr->size > ASTEROID_MIN_SIZE) {
                    CGPoint pt = [astr center];
                    // split it up
                    [self addAstrWithProps: astr->size/2 x:pt.x y:pt.y dx:astr->dx*ASTEROID_SPEEDUP dy:astr->dy*ASTEROID_SPEEDUP da:astr->da];
                    [self addAstrWithProps: astr->size/2 x:pt.x y:pt.y dx:-astr->dx*ASTEROID_SPEEDUP dy:-astr->dy*ASTEROID_SPEEDUP da:-astr->da];
                }

                // spawn new asteroids every 5 hits
                // an asteroid takes 7 hits to destroy, so this will slowly
                // increase the difficulty
                if(score%50 == 0) {
                    [self addAstr];
                }

                break;
            }
        }
    }
    
    // check for asteroid collisions if the ship is vulnerable
    if(ship.alpha >= 1.0) {
        for(UIView *astr in astrs){
            if(CGRectContainsRect([astr frame], [ship frame])){
                lives--;
                livesLB.text = [NSString stringWithFormat:@"%d",lives];
                [self playSound:@"bangSmall"];
                ship.alpha = 0.0; // makes ship invulnerable
            }
        }
        
        //game over check
        if(lives == 0){
            active = false;
            gameOver.hidden = NO;
            scoreFinal.text = [NSString stringWithFormat:@"%d", score];
            [timer invalidate];
            gamingScreen.hidden = YES;
            [self.backgroundMusic stop];
            [self playSound:@"go"]; //plays game over sound
        }
    }
    else {
        ship.alpha += 0.01;
    }
}

-(void)clearDebris{
    for(ObjectView* astr in astrs) {
        [astr removeFromSuperview];
    }
    for(ObjectView* bullet in bullets) {
        [bullet removeFromSuperview];
    }
    [astrs removeAllObjects];
    [bullets removeAllObjects];
}

-(IBAction)playAgain:(id)sender{
    [self clearDebris];
    [self startAnimation];
}

@end
