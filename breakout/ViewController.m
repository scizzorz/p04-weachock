//
//  ViewController.m
//  breakout
//
//  Created by Patrick Madden on 2/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
@property(nonatomic, strong) AVAudioPlayer *backgroundMusic;
@end

@implementation ViewController
@synthesize gameScreen, welcome, playBT, howTo, credits;

- (void)viewDidLoad {
	[super viewDidLoad];
    howTo.hidden = YES;
    credits.hidden = YES;
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [howTo addGestureRecognizer:singleFingerTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    [credits addGestureRecognizer:singleTap];
    
	// Do any additional setup after loading the view, typically from a nib.
    NSURL *musicFile = [[NSBundle mainBundle] URLForResource:@"intro"
                                               withExtension:@"mp3"];
    self.backgroundMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile
                                                                  error:nil];
    self.backgroundMusic.numberOfLoops = -1;
    [self.backgroundMusic play];
    gameScreen.hidden = YES;
	//[gameScreen createPlayField];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(IBAction)start:(id)sender{
    welcome.hidden = YES;
    gameScreen.hidden = NO;
    [self.backgroundMusic stop];
    [gameScreen startAnimation];
}

-(IBAction)activateHowToScr:(id)sender{
    howTo.hidden = NO;
}

-(IBAction)activateCreditsScr:(id)sender{
    credits.hidden = NO;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    howTo.hidden = YES;
}

- (void)singleTap:(UITapGestureRecognizer *)recognizer {
    credits.hidden = YES;
}

@end
