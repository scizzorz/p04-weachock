#import "ObjectView.h"

#define PI 3.1415926535
#define D2R(x) ((x) / 180.0 * PI)
#define R2D(x) ((x) / PI * 180.0)

@interface ObjectView ()
@end

@implementation ObjectView

-(void)move {
    CGPoint cur = [self center];
    cur.x += dx;
    cur.y += dy;
    [self rotate: angle + da];
    [self setCenter: cur];
}

-(void)rotate: (float)_angle {
    self.transform = CGAffineTransformMakeRotation(_angle);
    angle = _angle;
}

-(void)setVel: (float)_dx y:(float)_dy {
    dx = _dx;
    dy = _dy;
}

-(void)setRotVel: (float)_da {
    da = _da;
}

@end
