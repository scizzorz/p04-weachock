#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
@interface GameScreen : UIView {
    CGPoint target;
    CGPoint down;
    CGPoint down_target;
    bool active;
    int lives;
    int score;
}
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIView *ship;
@property (nonatomic, strong) NSMutableArray *astrs;
@property (nonatomic, strong) NSMutableArray *bullets;
@property (nonatomic, strong) IBOutlet UILabel *scoreLB, *livesLB, *scoreFinal;
@property (nonatomic, strong) IBOutlet UIView *gameOver, *gamingScreen;
@property (nonatomic, strong) IBOutlet UIButton *playAgainBt;


-(void)createPlayField;
-(void)startAnimation;
-(void)reset;
-(void)addAstr;
-(void)addAstrWithProps: (int)size x:(float)x y:(float)y dx:(float)dx dy:(float)dy da:(float)da;
-(void)playSound:(NSString*)soundName;
-(void)rotate: (float)angle;
@end
